import React from 'react'
import { Alert, Button, Col, Container, Row } from 'reactstrap';
import useAuth from '../../hooks/useAuth';
import useForm from '../../hooks/useForm';
import './Login.css';

const Login = () => {

    const { onChange, email, password } = useForm({ email: '', password: '' });
    const { login, loginError, setLoginError } = useAuth();

    const handleSubmit = ( e: any ) => {
        e.preventDefault();
        login( email, password );
    }

    return (
        <div className='background' style={{ height: window.innerHeight}}>
            <Container className='pt-5'>
                <Row className='shadow rounded'>
                    <Col className='login-panel__image-panel d-none d-lg-block' md={5} xl={6}>

                    </Col>
                    <Col className='login-panel__form-panel pb-5 px-5'>
                        <h2 className='fw-bold text-center py-5 text-light'>Bienvenido</h2>
                        <form onSubmit={handleSubmit}>
                            <Row className='mb-4'>
                                <Col>
                                    <label htmlFor="email" className="form-label text-light">Email</label>
                                    <input 
                                        type={'email'} 
                                        className="form-control" 
                                        name="email" 
                                        value={email} 
                                        onChange={ ({ target }) => onChange( target.value, 'email')}
                                    />
                                </Col>
                            </Row>
                            <Row className='mb-4'>
                                <Col>
                                    <label htmlFor="password" className="form-label text-light">Password</label>
                                    <input 
                                        type={'password'} 
                                        className="form-control" 
                                        name="password" 
                                        value={password} 
                                        onChange={ ({ target }) => onChange( target.value, 'password')}
                                    />
                                    { loginError && (
                                        <Alert
                                            color="danger"
                                            toggle={ () => setLoginError(false) }
                                        >
                                            {'Email or Password incorrect!'}
                                      </Alert>
                                    ) }
                                </Col>
                            </Row>
                            <Row className='mb-4 form-check'>
                                <Col>
                                    <input type={'checkbox'} className="form-check-input" name="connected" />
                                    <label htmlFor="connected" className="form-check-label text-light">Mantenerme conectado</label>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Button color='primary' block type='submit'>Iniciar Sesión</Button>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Login;
