import React, { useEffect, useState } from 'react';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import { Button, Card, CardBody, CardSubtitle, CardTitle } from 'reactstrap';
import reqResApi from '../../api/reqRes';
import { User } from '../../interfaces/userRes';
import { connect } from 'react-redux';
import { deleteUser, setUsers } from '../../actions';
import AppTable from '../../components/AppTable';
import './UserMaintenance.css';
import { GridColDef, GridRenderCellParams } from '@mui/x-data-grid';
import UserModal from '../../components/UserModal';
import { DrawerMenu } from '../../components/DrawerMenu';

const UserMaintenance = ( { users, userLogin, setUsers, deleteUser }: any ) => {
  const [modal, setModal] = useState(false);
  const [itemSelected, setItemSelected] = useState<User>({});

  const [dialogOpen, setDialogOpen] = useState(false);

  const toggleModal = () => setModal(!modal);

  const handleDialogClose = () => {
    setDialogOpen(false);
  };


  const chargeUsers = async () => {
    const response = await reqResApi.get('/users');
    setUsers( response.data );
  }

  const editItem = ( rowData: User) => {
    setItemSelected(rowData);
    toggleModal();
  }

  const alertUserDelete = ( userData: User) => {
    setItemSelected(userData);
    setDialogOpen(true);
  }

  const destroyUser = () => {
    deleteUser(itemSelected);
    setDialogOpen(false);
}

  const columns: GridColDef[] = [
    { 
      field: 'action', 
      headerName: 'Action',
      width: 170,
      renderCell: ( params: GridRenderCellParams) => (
        <>
          <Button color='primary' onClick={() => editItem(params.row)}>Edit</Button>&nbsp;
          <Button disabled={userLogin.id === params.row.id} color='danger' onClick={() => alertUserDelete(params.row)}>Delete</Button>
        </>
      ) 
    },
    { 
      field: 'id', 
      headerName: 'ID' ,
      width: 50
    },
    {
      field: 'firstName',
      headerName: 'First name',
      width: 150
    },
    {
      field: 'lastName',
      headerName: 'Last name',
      width: 150
    },
    {
      field: 'email',
      headerName: 'Email',
      width: 250
    },
    {
      field: 'username',
      headerName: 'Username',
      width: 150
    },
    {
      field: 'avatar',
      headerName: 'Avatar',
      width: 170,
      align: 'center',
      headerAlign: 'center',
      renderCell: ( params: GridRenderCellParams) => <img style={{width: 100}} src={params.value} />
    }
  ];



  useEffect( () => {

    chargeUsers();

  }, [])
  
  return (
    <div className='table__container background px-5'>
      <DrawerMenu />
      <Card className=' ml-5 mt-100'>
        <CardBody>
          <CardTitle tag="h1">
            Users
          </CardTitle>
          <CardSubtitle
            className="mb-2 text-muted"
            tag="h6"
          >
            Users Maintenance
          </CardSubtitle>
          <AppTable 
            columns={columns}
            rows={users}
          />
        </CardBody>
      </Card>
      <UserModal 
        toggleModal={toggleModal}
        isOpen={modal}
        item={itemSelected}
        setItemSelected={setItemSelected}
      />
      <Dialog
        open={dialogOpen}
        onClose={handleDialogClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Are you sure to delete this item?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {`The user ${itemSelected.firstName} ${itemSelected.lastName} will be delete`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialogClose} color="primary">
            Cancel
          </Button>
          <Button onClick={destroyUser} color="danger" autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </div>
    
  )
}

const mapStateToProps = ( state: any ) => {
  return {
    users: state.users,
    userLogin: state.userLogin
  }
}

const mapDispatchToProps = {
  setUsers,
  deleteUser
}
export default connect( mapStateToProps, mapDispatchToProps )(UserMaintenance);
