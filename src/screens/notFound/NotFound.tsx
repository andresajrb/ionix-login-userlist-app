import React from 'react';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import { Typography } from '@material-ui/core';

const NotFound = () => {
  return (
    <div className='background text-center pt-5'>
      <SentimentVeryDissatisfiedIcon style={{fontSize: '25vw'}}/>
      <Typography variant="h1" component="div" gutterBottom>
        404
      </Typography>
      <Typography variant="h3" component="div" gutterBottom>
        Page not Found
      </Typography>
    </div>
  )
}

export default NotFound;