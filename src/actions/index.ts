import { User } from "../interfaces/userRes";

export const setUsers = ( payload: User[] ) => ({
    type: 'SET_USERS',
    payload
});

export const addUser = ( payload: User ) => ({
    type: 'ADD_USER',
    payload
});

export const editUser = ( payload: User ) => ({
    type: 'EDIT_USER',
    payload
});

export const deleteUser = ( payload: User ) => ({
    type: 'DELETE_USER',
    payload
});

export const setUserLogin = ( payload: User ) => ({
    type: 'SET_USER_LOGIN',
    payload
});