import { createStore } from "redux";
import { State } from "../interfaces/state";
import reducer from "../reducers";

const authLogged = window.localStorage.getItem('authLogged') || null;
const authLoggedObject = authLogged ? JSON.parse(authLogged) : {}

const initialState: State = {
  users: [],
  userLogin: authLoggedObject
};


const store = createStore( reducer, initialState);

export default store;