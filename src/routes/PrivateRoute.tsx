import React from 'react'
import { connect, useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';


const PrivateRoute = ( { component: Component, ...rest}: any ) => {
    const userLogin = useSelector( ( state: any ) => state.userLogin );
    const isAuth = Object.entries(userLogin).length !== 0;
    return (
        <Route
            {...rest}
            render={(props) =>
                isAuth ? <Component {...props} /> : <Redirect to="/login" />
            }
        />
    )
}

// const mapStateToProps = ( state: any ) => {
//     return {
//         userLogin: state.userLogin
//     }
// }

export default PrivateRoute;

