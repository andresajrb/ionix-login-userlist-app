import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Login from '../screens/login/Login';
import NotFound from '../screens/notFound/NotFound';
import UserMaintenance from '../screens/users/UserMaintenance';
import PrivateRoute from './PrivateRoute';

const AppRouter = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/login" component={Login} /> 
            <PrivateRoute exact path="/users" component={UserMaintenance} /> 
            <PrivateRoute exact path="/" component={UserMaintenance} /> 
            <Route component={NotFound} /> 
        </Switch>
    </BrowserRouter>
)

export default AppRouter;