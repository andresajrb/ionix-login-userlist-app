import React from 'react'
import { AppBar, Avatar, CssBaseline, Divider, Drawer, IconButton, List, ListItem, ListItemIcon, ListItemText, Toolbar, Typography } from '@material-ui/core'
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import clsx from 'clsx';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import UserModal from './UserModal';
import useDrawerMenu from '../hooks/useDrawerMenu';
import useMaintModal from '../hooks/useMaintModal';
import useProfile from '../hooks/useProfile';
import useAuth from '../hooks/useAuth';

export const DrawerMenu = () => {

    const { classes, open, theme, handleDrawerOpen, handleDrawerClose } = useDrawerMenu();
    const { userProfile } = useProfile();
    const { modal, itemSelected, setItemSelected, toggleModal } = useMaintModal();

    const { logout } = useAuth();

    return (
        <>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap>
                        Test App
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
            <div className={classes.drawerHeader}>
                <IconButton onClick={handleDrawerClose}>
                {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </IconButton>
            </div>
            <Divider />
            <List>
                <ListItem button onClick={toggleModal}>
                    <ListItemIcon><Avatar alt="avatar" src={userProfile.avatar} /></ListItemIcon>
                    <ListItemText primary={`${userProfile.firstName} ${userProfile.lastName}`} />
                </ListItem>
            </List>
            <Divider />
            <List>
                <ListItem button onClick={logout}>
                    <ListItemIcon><ExitToAppIcon/></ListItemIcon>
                    <ListItemText primary={'Logout'} />
                </ListItem>
            </List>
            </Drawer>
            <UserModal
                toggleModal={toggleModal}
                isOpen={modal}
                item={itemSelected}
                setItemSelected={setItemSelected}
            />
        </>
    )
}
