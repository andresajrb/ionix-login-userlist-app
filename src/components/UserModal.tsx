import React from 'react';
import { useDispatch } from 'react-redux';
import { Button, Col, Container, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import { editUser } from '../actions';
import { User } from '../interfaces/userRes';

interface Props {
    toggleModal(): any;
    isOpen: boolean;
    item: User;
    setItemSelected: React.Dispatch<React.SetStateAction<User>>;
}

const UserModal = ( { toggleModal, isOpen, item, setItemSelected }: Props ) => {
    const dispatch = useDispatch();

    const handleUserChange = ( value: string, field: string) => {
        setItemSelected({
            ...item,
            [field]: value
        });
    }

    const saveUser = () => {
        dispatch(editUser(item));
        toggleModal();
    }

    return (
        <Modal
        toggle={toggleModal}
        isOpen={isOpen}
        className= 'mt-100'
      >
        <ModalHeader toggle={toggleModal}>
          User
        </ModalHeader>
        <ModalBody>
          <Form>
            <Container>
              <Row>
                <Col xs={6}>
                  <FormGroup>
                    <Label for="formId">
                      ID
                    </Label>
                    <Input
                      id="formId"
                      name="id"
                      placeholder="with a placeholder"
                      type="text"
                      value={item.id}
                      disabled
                    />
                  </FormGroup>
                </Col>
                <Col xs={6}>
                  <FormGroup>
                    <Label for="formUsername">
                      Username
                    </Label>
                    <Input
                      id="formUsername"
                      name="username"
                      placeholder="with a placeholder"
                      type="text"
                      value={item.username}
                      onChange={({ target }) => handleUserChange( target.value, 'username')}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={6}>
                  <FormGroup>
                    <Label for="formFirstName">
                      First Name
                    </Label>
                    <Input
                      id="formFirstName"
                      name="firstName"
                      placeholder="with a placeholder"
                      type="text"
                      value={item.firstName}
                      onChange={({ target }) => handleUserChange( target.value, 'firstName')}
                    />
                  </FormGroup>
                </Col>
                <Col xs={6}>
                  <FormGroup>
                    <Label for="formLastName">
                      Last Name
                    </Label>
                    <Input
                      id="formLastName"
                      name="lastName"
                      placeholder="with a placeholder"
                      type="text"
                      value={item.lastName}
                      onChange={({ target }) => handleUserChange( target.value, 'lastName')}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={6}>
                  <FormGroup>
                    <Label for="formEmail">
                      Email
                    </Label>
                    <Input
                      id="formEmail"
                      name="email"
                      placeholder="with a placeholder"
                      type="text"
                      value={item.email}
                      onChange={({ target }) => handleUserChange( target.value, 'email')}
                    />
                  </FormGroup>
                </Col>
                <Col xs={6}>
                  <FormGroup>
                    <Label for="formAvatar">
                      Avatar
                    </Label>
                    <Input
                      id="formAvatar"
                      name="avatar"
                      placeholder="with a placeholder"
                      type="text"
                      value={item.avatar}
                      onChange={({ target }) => handleUserChange( target.value, 'avatar')}
                    />
                  </FormGroup>
                </Col>
              </Row>
            </Container>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={saveUser}
          >
            Save
          </Button>
          {' '}
          <Button onClick={toggleModal}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    )
}

export default UserModal;
