import * as React from 'react';
import { DataGrid, GridColDef } from '@mui/x-data-grid';


interface TableProps {
  columns: GridColDef[];
  rows: Object[];
}

const AppTable = ({ columns, rows }: TableProps) => {
  return (
    <div className='app-table__container'>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        disableColumnMenu
        disableSelectionOnClick
        className='app-table__data-grid'
      />
    </div>
  );
}

export default AppTable;