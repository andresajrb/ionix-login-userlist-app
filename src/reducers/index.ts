import { State } from "../interfaces/state";
import { User } from "../interfaces/userRes";

type Action = 
    | { type: 'SET_USERS', payload: User[] }
    | { type: 'ADD_USER', payload: User }
    | { type: 'EDIT_USER', payload: User }
    | { type: 'SET_USERS', payload: User[] }
    | { type: 'SET_USER_LOGIN', payload: User }
    | { type: 'DELETE_USER', payload: User }

const reducer = ( state: any, action: Action ) => {
    switch (action.type) {
        case 'SET_USERS':
            return {
                ...state,
                users: [ ...action.payload ]
            };

        case 'ADD_USER':
            return {
                ...state,
                users: [ ...state.users, action.payload ]
            };

        case 'EDIT_USER':
            return {
                ...state,
                users: state.users.map( ( user: { id: number | undefined; } ) => (user.id === action.payload.id ? action.payload : user) )
            };

        case 'DELETE_USER':
            return {
                ...state,
                users: state.users.filter( (user: { id: number | undefined; }) => user.id !== action.payload.id )
            };

        case 'SET_USER_LOGIN':
            return {
                ...state,
                userLogin: action.payload
            }
    
        default:
            return state;
    }
}

export default reducer;