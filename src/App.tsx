import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './screens/login/Login';
import AppRouter from './routes/AppRouter';

function App() {
  return (
      <AppRouter />
  );
}

export default App;
