import React, { useState } from 'react'
import { User } from '../interfaces/userRes';
import reqResApi from '../api/reqRes';
import { useDispatch } from 'react-redux';
import { setUserLogin } from '../actions';
import { useHistory } from 'react-router-dom';

const useAuth = () => {
    const [loginError, setLoginError] = useState(false);
    
    const dispatch = useDispatch();
    const history = useHistory();

    const login = ( email: string, password: string ) => {
        reqResApi.post('/login', { email, password } ).then( resp => {
            const result: User = resp.data.result;
            if (resp.data.ok) {
                window.localStorage.setItem('authLogged', JSON.stringify(result) );
                dispatch( setUserLogin( result ) );
                history.push('/users');
                return;
            }
            setLoginError(true);
        }).catch( error => {
            setLoginError(true);
            console.log('Error', error);
        })
    }

    const logout = () => {
        dispatch( setUserLogin({}) );
        window.localStorage.removeItem('authLogged');
        history.push('/login');
    }

    return {
        loginError,
        login,
        logout,
        setLoginError
    }
}

export default useAuth