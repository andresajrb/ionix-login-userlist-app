import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { User } from '../interfaces/userRes';

const useMaintModal = () => {
    const userLogin = useSelector( (state: any) => state.userLogin );
    const [modal, setModal] = useState(false);
    const [itemSelected, setItemSelected] = useState<User>(userLogin);

    const toggleModal = () => setModal(!modal);

    return {
        modal,
        itemSelected,
        setItemSelected,
        toggleModal
    }
}

export default useMaintModal;