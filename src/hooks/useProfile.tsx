import React from 'react'
import { useSelector } from 'react-redux';
import { User } from '../interfaces/userRes';

const useProfile = () => {
    const userLogin = useSelector( (state: any) => state.userLogin );
    const users = useSelector( (state: any) => state.users );
    const userProfile = users.length === 0 ? userLogin : users.find( (user: User) => user.id === userLogin.id);

    return {
        userProfile
    }
}

export default useProfile