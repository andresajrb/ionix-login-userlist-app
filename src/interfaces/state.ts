import { User } from "./userRes";

export interface State {
    users: User[],
    userLogin: User
}