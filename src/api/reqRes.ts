
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { loginValidation } from './apiFunctions';
import userData from './userData';

const mock = new MockAdapter(axios, { delayResponse: 600 });

mock.onGet('/users').reply(250, userData);
mock.onPost('/login').reply( ( config ): any => {
    const loginData = JSON.parse(config.data);
    const loginUserValidate = loginValidation(loginData);
    return !!loginUserValidate 
        ? [ 200, { ok: true, message: 'login accepted', result: loginUserValidate } ] 
        : [ 200, { ok: false, message: 'login rejected' } ] ;
} )

export default axios;