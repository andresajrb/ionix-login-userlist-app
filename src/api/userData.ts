import { User } from '../interfaces/userRes';

export default <User[]> [
    {
        id: 1,
        firstName: 'Goldina',
        lastName: 'Daid',
        email: 'gdaid0@surveymonkey.com',
        username: 'gdaid0',
        avatar: 'http://dummyimage.com/100x100.png/ff4444/ffffff',
    }, {
        id: 2,
        firstName: 'Simonne',
        lastName: 'Brammer',
        email: 'sbrammer1@va.gov',
        username: 'sbrammer1',
        avatar: 'http://dummyimage.com/100x100.png/ff4444/ffffff'
    }, {
        id: 3,
        firstName: 'Shandra',
        lastName: 'Birk',
        email: 'sbirk2@goo.ne.jp',
        username: 'sbirk2',
        avatar: 'http://dummyimage.com/100x100.png/dddddd/000000'
    }, {
        id: 4,
        firstName: 'Jamaal',
        lastName: 'Danks',
        email: 'jdanks3@mapquest.com',
        username: 'jdanks3',
        avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff'
    }, {
        id: 5,
        firstName: 'Niall',
        lastName: 'Deadman',
        email: 'ndeadman4@cam.ac.uk',
        username: 'ndeadman4',
        avatar: 'http://dummyimage.com/100x100.png/ff4444/ffffff'
    }, {
        id: 6,
        firstName: 'Anthea',
        lastName: 'Salvadore',
        email: 'asalvadore5@hexun.com',
        username: 'asalvadore5',
        avatar: 'http://dummyimage.com/100x100.png/dddddd/000000'
    }, {
        id: 7,
        firstName: 'Pepi',
        lastName: 'Wrangle',
        email: 'pwrangle6@blinklist.com',
        username: 'pwrangle6',
        avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff'
    }, {
        id: 8,
        firstName: 'Shelby',
        lastName: 'Aronow',
        email: 'saronow7@bigcartel.com',
        username: 'saronow7',
        avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff'
    }, {
        id: 9,
        firstName: 'Austin',
        lastName: 'Scotchbourouge',
        email: 'ascotchbourouge8@weebly.com',
        username: 'ascotchbourouge8',
        avatar: 'http://dummyimage.com/100x100.png/dddddd/000000'
    }, {
        id: 10,
        firstName: 'Edan',
        lastName: 'Gilcriest',
        email: 'egilcriest9@lulu.com',
        username: 'egilcriest9',
        avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff'
    }, {
        id: 11,
        firstName: 'Sue',
        lastName: 'McComas',
        email: 'smccomasa@earthlink.net',
        username: 'smccomasa',
        avatar: 'http://dummyimage.com/100x100.png/ff4444/ffffff'
    }, {
        id: 12,
        firstName: 'Clive',
        lastName: 'Filby',
        email: 'cfilbyb@ezinearticles.com',
        username: 'cfilbyb',
        avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff'
    }, {
        id: 13,
        firstName: 'Gaelan',
        lastName: 'Goudge',
        email: 'ggoudgec@booking.com',
        username: 'ggoudgec',
        avatar: 'http://dummyimage.com/100x100.png/dddddd/000000'
    }, {
        id: 14,
        firstName: 'Maybelle',
        lastName: 'Fanton',
        email: 'mfantond@acquirethisname.com',
        username: 'mfantond',
        avatar: 'http://dummyimage.com/100x100.png/cc0000/ffffff'
    }, {
        id: 15,
        firstName: 'Elizabeth',
        lastName: 'Larkcum',
        email: 'elarkcume@nhs.uk',
        username: 'elarkcume',
        avatar: 'http://dummyimage.com/100x100.png/dddddd/000000'
    }, {
        id: 16,
        firstName: 'Waldon',
        lastName: 'Fierro',
        email: 'wfierrof@ning.com',
        username: 'wfierrof',
        avatar: 'http://dummyimage.com/100x100.png/dddddd/000000'
    }, {
        id: 17,
        firstName: 'Timmie',
        lastName: 'Isacq',
        email: 'tisacqg@google.nl',
        username: 'tisacqg',
        avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff'
    }, {
        id: 18,
        firstName: 'Hamish',
        lastName: 'Quodling',
        email: 'hquodlingh@google.com.hk',
        username: 'hquodlingh',
        avatar: 'http://dummyimage.com/100x100.png/ff4444/ffffff'
    }, {
        id: 19,
        firstName: 'Hoyt',
        lastName: 'Wroath',
        email: 'hwroathi@comcast.net',
        username: 'hwroathi',
        avatar: 'http://dummyimage.com/100x100.png/dddddd/000000'
    }, {
        id: 20,
        firstName: 'Kellina',
        lastName: 'Berzon',
        email: 'kberzonj@admin.ch',
        username: 'kberzonj',
        avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff'
    }]