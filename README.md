# GUIA DE APP DE PRUEBA TÉCNICA IONIX
# Por: Andres Jose Reyes

## Requerimientos

+ Nodejs 14+
+ Npm 7+

## Instalación de dependencias

```
npm install
```

## Levantar aplicación

```
npm start
```

## Descripción de funcionalidad de la aplicación y arquitectura

La aplicacion utilizar las librerias "axios" y "axios-mock-adapter" para simular el consumo o peticion de servicios http.

La aplicacion consta de dos pantallas (login y mantenimiento de usuarios) para iniciar sesión debe ingresar un email valido, estos se pueden encontrar en la carpeta "src/api/userData.ts" para mas velocidad puede ingresar directamente el email "gdaid0@surveymonkey.com".

La aplicación maneja el estado a través de las librerias "redux" y "react-redux", al iniciar sesión el usuario es guardado en el estado global y en el Storage del navegador. 

Las rutas de la aplicacion son gestionadas a traves de la libreria "react-router" en su version 5, esta posee una configuracion para mantener la ruta del mantenimiento de usuarios restringida sin haber iniciado sesión siempre redireccionando a la pantalla principal de login.

La interfaz del mantenimiento de usuarios esta construida con las librerias "bootstrap", "reactstrap" y "material-ui v5", la tabla utilizada es un componente Data Grid que proviene de Material UI.

El Drawer Menu tipo hamburguesa proviene de Material UI y en este se puede reflejar la imagen como un avatar del usuario el cual se actualiza automaticamente al actualizar la tabla de usuarios. Tambien se puede editar accionando click encima del nombre de usuario o el avatar.

La opcion de logout elimina tanto de state como del storage del navegador el usuario que inicio sesion restringiendo las rutas nuevamente.

## Posibles mejoras

+ Se puede dejar como nota de mejora la falta de test unitarios en la prueba, esta no se pudo incluir por falta de tiempo.
+ Tipado mas estricto en los componentes.
+ Manejo y simulacion de errores en el consumo de API y actualizacion de state a traves de los actions.

# Ejercicio 2

+ Primero, se tiene que tener en cuenta que no se debe utilizar un metodo de peticion GET debido a que los datos serian transmitidos a traves de la URI
+ Segundo, no se puede enviar la data sensible en texto plano a traves de la red, ya que estos pueden ser capturados.
+ Tercero, encriptar la data sensible desde el navegador y validar match en el servidor.